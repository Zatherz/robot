CRYSTAL ?= crystal
SRC_FILES = src/bot.cr src/config.yml src/plugin.cr

all: $(SRC_FILES)
	crystal deps install
	crystal build $< -o robot --release

clean:
	rm robot

.PHONY: all clean
