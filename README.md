# Robot

An extremely modular Crystal bot.

## Building

`make`

## Usage

There's a file called `config_base.yml` in the `src` directory.  
Rename it to `config.yml`, then open it and edit the values it tells you to.

After you do that, just run `./robot`.

## License

Licensed under the conditions in the LICENSE file. Most parser code is based on code licensed under the conditions in the PARSER\_LICENSE file.

## Development

TODO: Write development instructions here

## Contributing

1. [Fork it](https://gitlab.com/Zatherz/robot/forks/new)
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Merge Request

## Contributors

- [Zatherz](https://gitlab.com/u/Zatherz) - creator, maintainer
