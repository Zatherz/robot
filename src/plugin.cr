module Robot
  class Plugin
    @@plugins : Array(Plugin) = [] of Plugin

    def self.register(plugin : Plugin)
      @@plugins << plugin
    end

    def self.plugins
      @@plugins
    end

    @author : String
    @name : String
    @description : String
    @license : String
    getter author
    getter name
    getter description
    getter license

    def client_init(client : Discord::Client, cache : Discord::Cache)
    end

    def self.register
      Plugin.register self.new
    end

    def initialize
      @author = @name = @description = @license = "Unknown"
    end
  end

  module ConfigPlugin(ConfigType)
    @config : ConfigType
    getter config
    CONFIG_BASE_PATH = "data"

    def self.create_save_path(name : String)
      File.join(__DIR__, CONFIG_BASE_PATH, name + ".bin")
    end

    def save_config
      raise "Plugin has no config (Nil config type)!" if ConfigType == Nil
      File.open(ConfigPlugin.create_save_path(self.class.name), "w") { |file| file.write @config.to_msgpack }
    end

    def load_config
      raise "Plugin has no config (Nil config type)!" if ConfigType == Nil
      @config = ConfigType.from_msgpack File.read(ConfigPlugin.create_save_path(self.class.name))
    end

    def initialize
      super
      if File.exists?(ConfigPlugin.create_save_path(self.class.name))
        @config = load_config
      else
        @config = ConfigType.new
      end
    end
  end
end
