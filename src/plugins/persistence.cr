require "msgpack"
require "./core/commandplugin" # CommandPlugin
private class CachedMessage
  MessagePack.mapping({
    id:       {type: UInt64},
    username: {type: String},
    content:  {type: String},
  })

  def initialize(@id, @username, @content)
  end
end

private class Config
  MessagePack.mapping({
    enabled_channels: {type: Hash(UInt64, Bool)},
    message_cache:    {type: Hash(UInt64, CachedMessage)},
  })

  def initialize
    @enabled_channels = Hash(UInt64, Bool).new
    @message_cache = Hash(UInt64, CachedMessage).new
  end
end

private class PersistencePlugin < Robot::Plugin
  include CorePlugin::CommandPlugin
  include Robot::ConfigPlugin(Config)

  def initialize
    super
    @name = "persistence"
    @author = "Zatherz"
    @description = <<-END
      Basic plugin for recovering removed messages.
    END

    command "enable_persistence_on", 1, 1 do |args|
      config.enabled_channels[args[0].to_u64] = true
      save_config
      "OK"
    end

    command "disable_persistence_on", 1, 1 do |args|
      config.enabled_channels.delete args[0].to_u64
      save_config
      "OK"
    end
  end

  def client_init(client, cache)
    client.on_message_create do |payload|
      if config.enabled_channels[payload.channel_id]?
        config.message_cache[payload.id] = CachedMessage.new(payload.author.id, payload.author.username, payload.content)
      end
      save_config
    end
    client.on_message_delete do |payload|
      server_id = cache.resolve_channel(payload.channel_id).guild_id.not_nil!
      my_id = client.get_current_user.id

      if config.enabled_channels[payload.channel_id]?
        if message = config.message_cache[payload.id]?
          if message.id == my_id && message.content.includes? "Recovered removed message:"
            client.create_message payload.channel_id, message.content
          else
            client.create_message payload.channel_id, "Recovered removed message:\n<@!#{message.id}>: #{message.content}"
          end
        else
          client.create_message payload.channel_id, "Message with ID #{payload.id} has been deleted, but it wasn't cached so it can't be recovered."
        end
      end
      save_config
    end
  end
end
PersistencePlugin.register
