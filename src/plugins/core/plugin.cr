require "./commandplugin"
require "./permission"
require "./config"
require "./parser/parser"

module CorePlugin
  class Plugin < Robot::Plugin
    include CommandPlugin
    include Robot::ConfigPlugin(Config)
    private def evaluate_argument(argument, msg, cache)
      String.build do |str|
        argument.contents.each do |arg|
          case arg
          when Parser::Command
            str << execute(arg, msg, cache)
          when Parser::Literal
            str << arg.content
          end
        end
      end
    end

    def execute(command : Parser::Command, msg : Discord::Message, cache : Discord::Cache)
      name = evaluate_argument(command.name, msg, cache)
      args = command.args.map { |arg| evaluate_argument(arg, msg, cache) }

      result = ""
      found_any_command = false
      Robot::Plugin.plugins.each do |plugin|
        if plugin.is_a? CommandPlugin
          if plugincommand = plugin.commands.try &.[name]?
            min_args = plugincommand.min_args ? plugincommand.min_args : -Float32::INFINITY
            max_args = plugincommand.max_args ? plugincommand.max_args : Float32::INFINITY
            if args.size < min_args.not_nil! || args.size > max_args.not_nil!
              if min_args == -Float32::INFINITY
                raise CommandError.new("Expected less than #{max_args} argument#{max_args == 1 ? "" : "s"}, received #{args.size}")
              elsif max_args == Float32::INFINITY
                raise CommandError.new("Expected at least #{min_args} argument#{min_args == 1 ? "" : "s"}, received #{args.size}")
              elsif min_args == max_args
                raise CommandError.new("Expected #{min_args} argument#{min_args == 1 ? "" : "s"}, received #{args.size}")
              else
                raise CommandError.new("Expected between #{min_args} and #{max_args} arguments, received #{args.size}")
              end
            end
            found_any_command = true
            guild = cache.resolve_guild(cache.resolve_channel(id: msg.channel_id).guild_id.not_nil!)
            member = cache.resolve_member(guild_id: guild.id, user_id: msg.author.id)
            is_commander = false
            is_staff = false
            member.roles.each do |role_id|
              role = cache.resolve_role(role_id)
              if role.name == config.commander_role_name
                is_staff = true
                break
              elsif role.name == config.staff_role_name
                is_staff = true
                break
              end
            end
            perm = Permission::Normal
            perm = Permission::Commander if is_commander
            perm = Permission::Staff if is_staff
            perm = Permission::ServerOwner if guild.owner_id == msg.author.id
            perm = Permission::BotOwner if Robot::Bot.instance.not_nil!.config["owner"]["id"].to_s.to_u64? == msg.author.id
            raise "Insufficient permission" if plugincommand.permission > perm
            result = plugincommand.exec.call(args, perm, msg, cache)
            break
          end
        end
      end
      raise "Command not found" if !found_any_command
      return "" if command.no_return
      result
    end

    def initialize
      super
      @name = "core"
      @author = "Zatherz"
      @description = <<-END
        The core Robot plugin.
        Implements a system for commands. Any plugin using commands must require ./core/commandplugin.
        Also implements commands like 'echo' and 'help'.
      END

      @prefix = "$"
      @parser = Parser::Parser.new

      command "get_config" do
        "```#{config.inspect}```"
      end

      command "echo", 1 do |args|
        args.join(" ")
      end

      command "permission" do |args, perm|
        perm.to_s
      end

      command "perm.set_commander_role", 1, 1, Permission::Staff do |args, perm, msg, cache|
        guild = cache.resolve_guild(cache.resolve_channel(id: msg.channel_id).guild_id.not_nil!)
        if args[0].to_u64?
          found_role_name = nil
          guild.roles.each do |role|
            if role.id == args[0].to_u64
              found_role_name = role.name
            end
          end
          raise "Couldn't find role name by ID" if !found_role_name
          name = found_role_name.not_nil!
        else
          name = args[0]
        end
        role_exists = false
        guild.roles.each do |role|
          if role.name == name
            role_exists = true
          end
        end
        raise "Invalid role!" if !role_exists

        config.commander_role_name = name
        save_config
        "OK"
      end

      command "perm.set_staff_role", 1, 1, Permission::ServerOwner do |args, perm, msg, cache|
        guild = cache.resolve_guild(cache.resolve_channel(id: msg.channel_id).guild_id.not_nil!)
        if args[0].to_u64?
          found_role_name = nil
          guild.roles.each do |role|
            if role.id == args[0].to_u64
              found_role_name = role.name
            end
          end
          raise "Couldn't find role name by ID" if !found_role_name
          name = found_role_name.not_nil!
        else
          name = args[0]
        end
        role_exists = false
        guild.roles.each do |role|
          if role.name == name
            role_exists = true
          end
        end
        raise "Invalid role!" if !role_exists

        config.staff_role_name = name
        save_config
        "OK"
      end
    end

    def client_init(client, cache)
      client.on_message_create do |msg|
        if msg.content.starts_with? @prefix
          ast = nil
          begin
            ast = @parser.parse msg.content
          rescue e : Parser::ParserError
            client.create_message msg.channel_id, "Parser error:\n```#{e.message || "[no exception message?]"}```"
            next
          end

          result = nil
          begin
            result = execute ast, msg, cache if !ast.nil?
          rescue e
            client.create_message msg.channel_id, "Error while running command:\n```#{e.message || "[no exception message?]"}```"
            next
          end
          if !ast.nil? && !result.nil?
            client.create_message msg.channel_id, result
          end
        end
      end
    end
  end
end

CorePlugin::Plugin.register
