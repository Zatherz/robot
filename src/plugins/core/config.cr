require "msgpack"
require "./permission"

module CorePlugin
  class Config
    MessagePack.mapping({
      commander_role_name: {type: String},
      staff_role_name:     {type: String},
    })

    def initialize
      @commander_role_name = "Robot's Overlord"
      @staff_role_name = "Staff"
    end
  end
end
